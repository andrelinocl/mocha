<?php include('elements/header.php');?>
<?php include('elements/menu-topo.php');?>
<?php include('elements/sidebar-menu.php');?>

<main>
    <section class="teste">
        <container>

            <h1>
                Cards
            </h1>
            <p>
                <code>{.card>.card-title+.card-body+.card-footer}</code>
            </p>

            <row>
                <column xs="12" sm="6">
                    <card>
                        <card-header>
                            <h2 class="card-title">
                                Card Title
                            </h2>
                        </card-header>
                        <card-body>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </card-body>
                        <card-footer>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        </card-footer>
                    </card>
                </column>
            </row>

            <p><br></p>
            <h5>Cards <i class="fa fa-angle-right"></i> <strong>Image Positions</strong> <small>[ left, center, right ]</small></h5>
            <p>
                Para alterar a posição da imagem é necessário que esteja circundada com a tag <code><'figure><'/figure></code>.
                Por padrão o posicionamento da imagem é centralizada, sendo assim desnecessário a utilização do data atributo {data-position="center"}</p>
            </p>
            <row>
                <column xs="12" sm="4">
                    <card>
                        <card-header>
                            <h5 class="card-title">LEFT</h5>
                        </card-header>
                        <card-body>
                            <figure data-position="left">
                                <img src="<?php echo BASE_URL;?>assets/images/pexels-photo-172033.jpeg" alt="">
                            </figure>
                        </card-body>
                    </card>
                </column>
                <column xs="12" sm="4">
                    <card>
                        <card-header>
                            <h5 class="card-title">CENTER</h5>
                        </card-header>
                        <card-body>
                            <figure data-position="center">
                                <img src="<?php echo BASE_URL;?>assets/images/pexels-photo-172033.jpeg" alt="">
                            </figure>
                        </card-body>
                    </card>
                </column>
                <column xs="12" sm="4">
                    <card>
                        <card-header>
                            <h5 class="card-title">RIGHT</h5>
                        </card-header>
                        <card-body>
                            <figure data-position="right">
                                <img src="<?php echo BASE_URL;?>assets/images/pexels-photo-172033.jpeg" alt="">
                            </figure>
                        </card-body>
                    </card>
                </column>
            </row>

            <p><br></p>
            <h5>Cards <i class="fa fa-angle-right"></i> <strong>Background Colors</strong> <small>[ .bg- ]</small></h5>
            <p>A cor de fundo do .card-header pode-se trocar através da classe <code>bg-{color}</code> ou passando data attr <code>[data-background="{color}"]</code></p>

            <row>
                <column xs="12" md="4">
                    <card>
                        <card-header class="bg-teal">
                            <h5 class="card-title">.bg-teal</h5>
                        </card-header>
                        <card-body>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.
                            </p>
                        </card-body>
                    </card>
                </column>
                <column xs="12" md="4">
                    <card>
                        <card-header data-background="brown">
                            <h5 class="card-title">data-background="brown"</h5>
                        </card-header>
                        <card-body>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.
                            </p>
                        </card-body>
                    </card>
                </column>
            </row>

            <p><br></p>
            <h5>Cards <i class="fa fa-angle-right"></i> <strong>Card Header Sizes </strong> <small>[ data-size="{small, regular, big}" ]</small></h5>
            <p>Altera-se o tamanho do .card-header</p>

            <row>
                <column xs="12" sm="4">
                    <card>
                        <card-header bg="pink" data-size="small" data-text-position="center">
                            <h5 class="card-title">SMALL</h5>
                        </card-header>
                        <card-body>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.
                            </p>
                        </card-body>
                    </card>
                </column>
                <column xs="12" sm="4">
                    <card>
                        <card-options bg="teal">
                            <li><a href="">Item</a></li>
                            <li><a href="">Item 2</a></li>
                            <li><a href="">Item 3</a></li>
                            <li><a href="">Item 4</a></li>
                            <li><a href="">Item</a></li>
                            <li><a href="">Item 2</a></li>
                            <li><a href="">Item 3</a></li>
                            <li><a href="">Item 4</a></li>
                        </card-options>
                        <card-header bg="teal" data-size="small" data-text-position="center" data-target=".card-options">
                            <h3 class="card-title">REGULAR</h3>
                            <a href="#" class="card-menu" data-target=".card-options">
                                <i class="fas fa-ellipsis-h"></i>
                            </a>
                        </card-header>
                        <card-body>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.
                            </p>
                        </card-body>
                    </card>
                </column>
                <column xs="12" md="4">
                    <card>
                        <card-options bg="purple">
                            <li><a href="">Item 1</a></li>
                            <li><a href="">Item 2</a></li>
                            <li><a href="">Item 3</a></li>
                            <li><a href="">Item 4</a></li>
                        </card-options>
                        <card-header data-size="big" data-text-position="center" data-background="true" style="background-image: url('<?php echo BASE_URL;?>assets/images/stranger-things-bicycle-lights-children.jpg');">
                            <h1 class="card-title white">BIG</h1>

                            <a href="#" class="card-menu" data-target=".card-options">
                                <i class="fas fa-ellipsis-h"></i>
                            </a>
                        </card-header>
                        <card-body>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.
                            </p>
                        </card-body>
                    </card>
                </column>

            </row><!-- end [ COLUMNS] -->

        </container>
    </section>
</main>

<?php include('elements/footer.php');?>
