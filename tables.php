<?php include('elements/header.php');?>
<?php include('elements/menu-topo.php');?>
<?php include('elements/sidebar-menu.php');?>

<main>
  <section class="teste">
    <div class="container">
      <h1>Tables</h1>
      <p>
        <code>{.table}</code>
      </p>

      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>nome</th>
            <th>email</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>John Dow</td>
            <td>jd@email.com</td>
          </tr>
          <tr>
            <td>2</td>
            <td>John Doe</td>
            <td>johndoe@email.com</td>
          </tr>
        </tbody>
      </table>

      <p><br /></p>
      <h5>Table > <strong>Hover e Striped</strong></h5>
      <p>Tabela com conteúdo editável</p>
      <p><code>{.table[data-style="striped"]}</code></p>
      <table class="table" data-style="striped">
        <thead>
          <tr>
            <th>#</th>
            <th>nome</th>
            <th>email</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>John Dow</td>
            <td>jd@email.com</td>
          </tr>
          <tr>
            <td>2</td>
            <td>John Doe</td>
            <td>johndoe@email.com</td>
          </tr>
        </tbody>
      </table>
      <p><br /></p>
      <p><code>{.table[data-style="striped-hover"]}</code> ou <code>{.table[data-style="hover-striped"]}</code></p>
      <table class="table" data-style="striped-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>nome</th>
            <th>email</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>John Dow</td>
            <td>jd@email.com</td>
          </tr>
          <tr>
            <td>2</td>
            <td>John Doe</td>
            <td>johndoe@email.com</td>
          </tr>
        </tbody>
      </table>
      <p><br /></p>
    </div><!-- end [ CONTAINER > TABLES > DATA-STYLE] -->

    <div class="container">
      <h5>Table > <strong>COLOR</strong></h5>
      <h6>Inverse</h6>
      <p><code>{table.inverse}</code></p>
      <table class="table inverse">
        <thead>
          <tr>
            <th>#</th>
            <th>nome</th>
            <th>email</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>John Dow</td>
            <td>jd@email.com</td>
          </tr>
          <tr>
            <td>2</td>
            <td>John Doe</td>
            <td>johndoe@email.com</td>
          </tr>
        </tbody>
      </table>
      <p><br></p>

      <p><code>{table.inverse[data-style="striped"]}</code></p>
      <table class="table inverse" data-style="striped">
        <thead>
          <tr>
            <th>#</th>
            <th>nome</th>
            <th>email</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>John Dow</td>
            <td>jd@email.com</td>
          </tr>
          <tr>
            <td>2</td>
            <td>John Doe</td>
            <td>johndoe@email.com</td>
          </tr>
        </tbody>
      </table>
      <p><br></p>

      <p><code>{.table.inverse[data-style="striped-hover"]}</code> ou <code>{.table.inverse[data-style="hover-striped"]}</code></p>
      <table class="table inverse" data-style="striped-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>nome</th>
            <th>email</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>John Dow</td>
            <td>jd@email.com</td>
          </tr>
          <tr>
            <td>2</td>
            <td>John Doe</td>
            <td>johndoe@email.com</td>
          </tr>
        </tbody>
      </table>
      <p><br></p>
    </div><!-- end [ CONTAINER > TABLE > COLOR ] -->

    <div class="container">
      <h5>Table > <strong>Forms</strong></h5>
      <p><code>{.table[data-color="color"]}</code></p>
      <div class="table inverse" data-form="true">
        <div class="thead">
          <div class="tr">
            <div class="th">#</div>
            <div class="th">nome</div>
            <div class="th">email</div>
            <div class="th">opções</div>
          </div>
        </div>
        <div class="tbody">
          <div class="tr">
            <div class="td">1</div>
            <div class="td">John Dow</div>
            <div class="td">jd@email.com</div>
            <div class="td buttons" data-size="small">
              <a href="#" class="button blue"><i class="fa fa-flag"></i></a>
              <a href="#" class="button deep-orange"><i class="fa fa-pencil"></i></a>
              <a href="#" class="button red"><i class="fa fa-trash-o"></i></a>
            </div>
          </div><!-- end [ .TR ] -->
          <div class="tr table-form">
            <div class="td" colspan="4">
              <form class="form" action="javascript:void(0)" method="post">
                <label>ID</label><input type="text" name="id" value="1" class="input" readonly>
                <label>Nome</label><input type="text" name="nome" value="John Dow" class="input">
                <label>E-mail</label><input type="text" name="email" value="jd@email.com" class="input">
                <div class="buttons">
                  <button type="submit" name="button" class="button blue"><i class="fa fa-floppy-o"></i></button>
                </div>
              </form>
            </div>
          </div><!-- end [ .TR ] -->
          <div class="tr">
            <div class="td">2</div>
            <div class="td">John Doe</div>
            <div class="td">johndoe@email.com</div>
            <div class="td buttons" data-size="small">
              <a href="#" class="button blue"><i class="fa fa-flag"></i></a>
              <a href="#" class="button deep-orange"><i class="fa fa-pencil"></i></a>
              <a href="#" class="button red"><i class="fa fa-trash-o"></i></a>
            </div>
          </div><!-- end [ .TR ] -->
        </div><!-- end [ .TBODY ] -->
      </div><!-- end [ DIV.TABLE ] -->
      <p><br></p>
    </div><!-- end [ CONTAINER > BREADCRUMB > ROUNDED ] -->
  </section>
</main>

<?php include('elements/footer.php');?>
