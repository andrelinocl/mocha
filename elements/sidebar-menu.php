<aside-menu inline-template>
    <aside class="side-nav bg-blue-gray-900 text-uppercase active" id="sidenav-menu">
        <ul>
            <li v-for="(m) in menus">
                <a href="javascript:void(0)"><i v-bind:class="m.icon"></i> {{ m.menu }}</a>
                <ul class="bg-blue-gray-900">
                    <li v-for="(s) in m.submenu">
                        <a v-bind:href="s.href"><i v-bind:class="s.icon"></i> {{ s.nome }}</a>
                    </li>
                </ul>
            </li>
        </ul>
    </aside>
</aside-menu>
