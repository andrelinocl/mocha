<nav class="main-nav blue-gray-900 fixed-top">
    <container class="fluid ml-xs-0 pl-xs-0">
        <div class="brand">
            <h3>
                <a href="<?php echo BASE_URL;?>">
                    <img src="/assets/images/icons/mocha.svg" alt="MOCHA">
                    MOCHA
                </a>
            </h3>
            <a href="javascript:void(0)" class="open-menu" data-target="#sidenav-menu">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <ul class="flex-end">
            <li><a href="">item a</a></li>
            <li><a href="">item b</a></li>
            <li><a href="">item c</a></li>
        </ul>
    </container>
</nav>
