    <footer>
        <p>Desenvolvido por André Carello</p>
    </footer>
</div>

    <!-- BEGIN: SCRIPTS -->
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/lib/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/lib/mask.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/lib/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://use.fontawesome.com/releases/v5.0.2/js/all.js"></script>

    <script type="text/javascript" src="https://unpkg.com/vue@2.5.13/dist/vue.js"></script>
    <script type="text/javascript" src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-router/3.0.1/vue-router.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL;?>assets/js/index.js"></script>

    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/main.js?<?php echo time();?>"></script>
    <!-- END: SCRIPTS -->
</body>
</html>
