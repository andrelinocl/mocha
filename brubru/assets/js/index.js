Vue.component('aside-menu', {
    data: function() {
        return {
            menus: []
        }
    },
    created: function() {
        axios
            .get(`http://mocha.local/server/aside-menu.json`).then(response => this.menus = response.data)
    }
});



var _container = Vue.extend({
    template : `<div class="container"><slot></slot></div>`
});

var _row = Vue.extend({
    template: `<div class="columns"><slot></slot></div>`
});


Vue.component('container', _container);
Vue.component('wrapper', _container);
Vue.component('columns', _row);
Vue.component('row', _row);
Vue.component('column', {
    template: `<div class="column" :class="{{classe}}"><slot></slot></div>`,
    data: function() {
        return {
            classe: ''
        }
    },
    props: [
        'xs',
        'sm',
        'md',
        'lg',
        'xl'
    ],
    created() {

        if(this.xs !== undefined && this.xs !== ''){
            classe = " xs-" + this.xs;
        }
        if(this.sm !== undefined && this.sm !== ''){
            classe = " sm-" + this.sm;
        }
        if(this.md !== undefined && this.md !== ''){
            classe = " md-" + this.md;
        }
        if(this.lg !== undefined && this.lg !== ''){
            classe = " lg-" + this.lg;
        }
        if(this.xl !== undefined && this.xl !== ''){
            classe = " xl-" + this.xl;
        }

        console.log(classe);
    }
});

new Vue({
    el: '#app',
});