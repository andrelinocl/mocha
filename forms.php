<?php include('elements/header.php');?>
<?php include('elements/menu-topo.php');?>
<?php include('elements/sidebar-menu.php');?>

<main>
  <section class="teste">
    <div class="container">
      <h1>Forms</h1>
      <form class="form" action="#" method="post" data-validation="true">
        <div class="columns">
          <div class="column xs-12">
            <label>Nome Completo</label>
            <input
              type="text"
              name="nome"
              class="input"
              required="required"
              data-rule-required="true"
              data-msg-required="Insira seu nome"
            >
          </div><!-- end [ COLUMN ] -->
        </div><!-- end [ COLUMNS ] -->
        <div class="columns">
          <div class="column xs-12 sm-6">
            <label>E-mail</label>
            <input
              type="email"
              name="email"
              class="input"
              required="required"
            >
          </div><!-- end [ COLUMN ] -->
          <div class="column xs-12 sm-6">
            <label>E-mail 2</label>
            <input
              type="email"
              name="email2"
              class="input"
              required="required"
            >
          </div><!-- end [ COLUMN ] -->
        </div><!-- end [ COLUMNS ] -->

        <div class="columns">
          <div class="column xs-12 sm-4">
            <label>Endereço</label>
            <input
              type="text"
              name="endereco"
              class="input"
              required="required"
            >
          </div><!-- end [ COLUMN ] -->
          <div class="column xs-12 sm-4">
            <label>Número</label>
            <input
              type="number"
              name="email"
              class="input"
              required="required"
            >
          </div><!-- end [ COLUMN ] -->
          <div class="column xs-12 sm-4">
            <label>Url</label>
            <input
              type="url"
              name="email"
              class="input"
              required="required"
            >
          </div><!-- end [ COLUMN ] -->
        </div><!-- end [ COLUMNS ] -->

      </form><!-- end [ FORM ] -->
    </div><!-- end [ CONTAINER ] -->
  </section>
</main>

<?php include('elements/footer.php');?>
