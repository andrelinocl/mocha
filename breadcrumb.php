<?php include('elements/header.php');?>
<?php include('elements/menu-topo.php');?>
<?php include('elements/sidebar-menu.php');?>

<main>
    <section>

        <container>
            <h1>Breadcrumb</h1>
            <p>
                <code>{.breadcrumb>li*n>a}</code>
            </p>

            <breadcrumb>
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Components</a></li>
                <li>Breadcrumb</li>
            </breadcrumb>

            <hr />
            <h5>Breadcrumb > <strong>Position</strong></h5>
            <p>Pode-se alterar o posicionamento do .breadcrumb adicionando data attr  <strong>data-position</strong> {left, center, right}</p>
            <p><code>{ul.breadcrumb[data-position="left"]}</code></p>
            <breadcrumb position="left">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Components</a></li>
                <li>Breadcrumb</li>
            </breadcrumb>

            <p><code>{ul.breadcrumb[data-position="center"]}</code></p>
            <breadcrumb position="center">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Components</a></li>
                <li>Breadcrumb</li>
            </breadcrumb>

            <p><code>{ul.breadcrumb[data-position="right"]}</code></p>
            <breadcrumb position="right">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Components</a></li>
                <li>Breadcrumb</li>
            </breadcrumb>
        </container>

        <div class="container">
            <h5>Breadcrumb > <strong>COLOR</strong></h5>
            <p>
                EX: <code>{.breadcrumb.inverse}</code>
            </p>
            <breadcrumb class="inverse" position="right">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Components</a></li>
                <li>Breadcrumb</li>
            </breadcrumb>
            <breadcrumb class="inverse" position="center">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Components</a></li>
                <li>Breadcrumb</li>
            </breadcrumb>
        </div><!-- end [ CONTAINER > BREADCRUMB > COLOR ] -->

    </section>
</main>

<?php include('elements/footer.php');?>
