Vue.component('aside-menu', {
    data: function() {
        return {
            menus: [],
            loading: true
        }
    },
    created: function() {
        axios
            .get(APP_ROOT + `server/aside-menu.json`).then(
                response => this.menus = response.data
            );
    }
});



var _container = Vue.extend({
    template : `<div :class="this.classe"><slot></slot></div>`,
    data: function() {
        return {
            classe : "container"
        }
    },
});

var _row = Vue.extend({
    template: `<div class="row"><slot></slot></div>`
});

Vue.component('container', _container);
Vue.component('wrapper', _container);
Vue.component('columns', _row);
Vue.component('row', _row);
Vue.component('column', {
    template: `<div :class="this.classe"><slot></slot></div>`,
    data: function() {
        return {
            classe: "column"
        }
    },
    props: ['xs', 'sm', 'md', 'lg', 'xl'],

    // CONVERTE PROPS ARRAY EM FUNÇÃO
    // props: function () {
    //   var t = [
    //     'xs', 'sm', 'md', 'lg', 'xl'
    //   ];
    //   var d = {
    //     type: String,
    //     default: function() {
    //       return "";
    //     }
    //   };
    //
    //   t.map(function(o, i){
    //     t[o] = d;
    //   });
    //
    //   return t;
    //
    // }(),
    created() {
        this.xs != undefined ? this.classe += " xs-"+this.xs : '';
        this.sm != undefined ? this.classe += " sm-"+this.sm : '';
        this.md != undefined ? this.classe += " md-"+this.md : '';
        this.lg != undefined ? this.classe += " lg-"+this.lg : '';
        this.xl != undefined ? this.classe += " xl-"+this.xl : '';
    }
});

Vue.component('breadcrumb', {
    template : `<ul :class="this.classe" :data-position="this.position"><slot></slot></ul>`,
    data: function() {
        return {
            classe: "breadcrumb"
        }
    },
    props: ['position']
});

Vue.component('card', {
    template : `<article :class="this.classe"><slot></slot></article>`,
    data: function() {
        return {
            classe: "card"
        }
    }
});
Vue.component('card-header', {
    template : `<header :class="this.classe"><slot></slot></header>`,
    props: ['bg'],
    data: function() {
        return {
            classe: "card-header"
        }
    },
    created() {
        console.log(this.bg);
        if(this.bg){
            this.classe += ' bg-'+this.bg
        }
    }
});
Vue.component('card-body', {
    template : `<section :class="this.classe"><slot></slot></section>`,
    data : function() {
        return {
            classe: "card-body"
        }
    }
});
Vue.component('card-footer', {
    template: `<div :class="this.classe"><slot></slot></div>`,
    data: function() {
        return {
            classe: "card-footer"
        }
    }
});
Vue.component('card-options', {
    template: `<div :class="this.classe"><ul><slot></slot></ul></div>`,
    props: ['bg'],
    data: function() {
        return {
            classe: "card-options",
        }
    },
    created() {
        if(this.bg){
            this.classe += ' bg-'+this.bg;
        }
    }
})

new Vue({
    el: '#app',
    data: function() {
        return {
            loading: true
        }
    }
});
