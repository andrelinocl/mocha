
setTimeout( function () {
    $(document).ready( function() {

        const menu = $('.open-menu'),
        asideMenu = $('aside.side-nav ul li a'),
        dest = $('main, footer');

        menu.click( function() {
            var _t = $(this),
            _data = _t.data('target');

            $(_data).toggleClass('active');

            if($(_data).hasClass('active')){
                dest.css({
                    'width' : $(window).width() - $(_data).width() + 'px',
                    'left'  : $(_data).width()
                })
            } else{
                dest.css({
                    'width' : $(window).width() + 'px',
                    'left'  : 0
                })
            }

        });

        asideMenu.click( function() {
            const _t = $(this);

            _t.siblings('ul').append('<li><a href="javascript:void(0)" class="asideclose"><i class="fa fa-arrow-left"></i></a></li>');
            _t.siblings('ul').addClass('active');

            $('.asideclose').click( function() {
                $(this).parents('ul.active').removeClass('active');
                $(this).remove();
            });
        });

        const dropup = $('.dropup>a');

        dropup.click( function(event) {
            event.preventDefault();
            var _t = $(this).parent(),
            _d = _t.find('.dropup-content');

            _t.toggleClass('active');

            if(_t.hasClass('active')){
                if(_t.data('size')<5){
                    _d.css({'height': _d.children().height() * _d.children().length + 'px', 'opacity': 1});
                } else{
                    _d.css({'height': _d.children().height() * _t.data('size') + 'px', 'opacity': 1});
                }


            } else {
                _d.css({'height': 0,'opacity':0});
            }
        });

        const form = $('.form');

        if(form.data('validate') === true){
            form.each( function() {
                $(this).validate();
            })
        }


        const breadcrumb = $('.breadcrumb');
        breadcrumb.each( function() {
            if($(this).data('icon')){
                $(this).find('li:before').css({'content': $(this).data('icon')});
            }
        });


        const cardmenu = $('.card-menu');
        cardmenu.click(function(event) {
            event.preventDefault();
            var t = $(this),
            v = t.parents('.card');

            v.toggleClass('active');
        });

    });

}, 500);
