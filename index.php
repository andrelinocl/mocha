<?php include('elements/header.php');?>
<?php include('elements/menu-topo.php');?>
<?php include('elements/sidebar-menu.php');?>

<main>

    <container>
        <row>
            <column xs="12" md="6">
                <h5>Criado usando o component container</h5>
            </column>
        </row>
    </container>

    <wrapper>
        <row>
            <column xs="5" md="3">
                <h5>Criado usando o component wrapper</h5>
            </column>
        </row>
    </wrapper>

  <div class="dropup fixed bg-teal" data-position="bottom-right" data-size="9">
    <a href="#" class="circle" data-target="#dropup-menu"><i class="fa fa-plus"></i></a>
    <ul class="dropup-content" id="dropup-menu">
      <li><a href="#">asd</a></li>
      <li><a href="#">asdasd</a></li>
      <li><a href="#">asd</a></li>
      <li><a href="#">asdasd</a></li>
      <li><a href="#">asd</a></li>
      <li><a href="#">asdasd</a></li>
      <li><a href="#">asd</a></li>
      <li><a href="#">asdasd</a></li>
      <li><a href="#">asd</a></li>
      <li><a href="#">asdasd</a></li>
    </ul>
  </div>
</main>


<!-- <div class="loader-content"><div class="loader"></div></div> -->

<?php include('elements/footer.php');?>
