<?php include('elements/header.php');?>
<?php include('elements/menu-topo.php');?>
<?php include('elements/sidebar-menu.php');?>

<main>

    <section class="grid-layout">

        <container>
            <h1>Grid</h1>
            <p>Este sistema de grid é compilado com a utilização do <a href="https://vuejs.org/" target="_blank">VUEJS</a></p>
            <p>
              <code>{.columns>.column.{xs,sm,md,lg,xl}-{1,2,3,4,5,6,7,8,9,10,11,12}}</code>
            </p>


            <row>
                <column sm="6" xl="8" class="qqc">
                    <p>.column.sm-6.xl-8</p>
                </column>
                <column sm="6" xl="4">
                    <p>.column.sm-6.xl-4</p>
                </column>
            </row>


            <p><br /></p>
            <h5>Grid <i class="fa fa-angle-right"></i> <strong>Nested Columns</strong></h5>
            <p>
              <code>{.columns>.column.{xs,sm,md,lg,xl}-{1,2,3,4...,11,12}>.column.{xs,sm,md,lg,xl}-{1,2,3,4...,11,12}}</code>
            </p>

            <row>
                <column xl="12">
                    <p>.column.xs-12.md-6.xl-4</p>

                    <row>
                        <column xs="12" md="6">
                            <p>.column.xs-12.md-6</p>
                        </column>
                        <column xs="12" md="6">
                            <p>.column.xs-12.md-6</p>
                        </column>
                    </row>
                </column>
            </row>


            <p><br /></p>
            <h5>Grid <i class="fa fa-angle-right"></i> <strong>Margin Columns</strong></h5>
            <p>
              <code>{.columns>.ml.{xs,sm,md,lg,xl}-{1,2,3,4...,11,12}>.column.{xs,sm,md,lg,xl}-{1,2,3,4...,11,12}}</code>
            </p>

            <row>
                <column xs="12" md="5" class="ml-md-7">
                    <p>.column.xs-12.md-5.ml-md-7</p>
                </column>
                <column xs="10" md="6" class="ml-md-3">
                    <p>.column.xs-10.md-6.ml-md-3</p>
                </column>
            </row>

        </container>

  </section>
</main>

<?php include('elements/footer.php');?>
