<?php include('elements/header.php');?>
<?php include('elements/menu-topo.php');?>
<?php include('elements/sidebar-menu.php');?>

<main>
  <section class="teste">
    <div class="container">
      <h1>Buttons</h1>
      <p>
        Todos os botões extendem da classe .button. EX:
        <code>{.button.pink}</code>
      </p>
      <a href="javascript:void(0)" class="button default">.default</a>
      <a href="javascript:void(0)" class="button red">.red</a>
      <a href="javascript:void(0)" class="button pink">.pink</a>
      <a href="javascript:void(0)" class="button purple">.purple</a>
      <a href="javascript:void(0)" class="button deep-purple">.deep-purple</a>
      <a href="javascript:void(0)" class="button indigo">.indigo</a>
      <a href="javascript:void(0)" class="button blue">.blue</a>
      <a href="javascript:void(0)" class="button light-blue">.light-blue</a>
      <a href="javascript:void(0)" class="button cyan">.cyan</a>
      <a href="javascript:void(0)" class="button teal">.teal</a>
      <a href="javascript:void(0)" class="button green">.green</a>
      <a href="javascript:void(0)" class="button light-green">.light-green</a>
      <a href="javascript:void(0)" class="button lime">.lime</a>
      <a href="javascript:void(0)" class="button yellow">.yellow</a>
      <a href="javascript:void(0)" class="button amber">.amber</a>
      <a href="javascript:void(0)" class="button orange">.orange</a>
      <a href="javascript:void(0)" class="button deep-orange">.deep-orange</a>
      <a href="javascript:void(0)" class="button brown">.brown</a>
      <a href="javascript:void(0)" class="button gray">.gray</a>
      <a href="javascript:void(0)" class="button gray-900">.gray-900</a>
      <a href="javascript:void(0)" class="button blue-gray">.blue-gray</a>
      <a href="javascript:void(0)" class="button blue-gray-900">.blue-gray-900</a>

      <hr />
      <h4><strong>Sizes</strong></h4>
      <p>Pode-se trocar o tamanho dos botões utilizando-se <strong>data-size</strong> {small, large}</p>
      <p><code>{.button.blue[data-size="small"]}</code></p>
      <a href="javascript:void(0)" class="button blue" data-size="small">.blue</a>
      <p><code>{.button.red[data-size="large"]}</code></p>
      <a href="javascript:void(0)" class="button red" data-size="large">.red</a>
      <p><br></p>
    </div><!-- end [ CONTAINER > BUTTONS] -->

    <div class="container">
      <h3><strong>ROUNDED</strong> Class</h3>
      <p>
        EX: <code>{.button.rounded}</code>
      </p>
      <a href="javascript:void(0)" class="button default rounded">.default</a>
      <a href="javascript:void(0)" class="button red rounded">.red</a>
      <a href="javascript:void(0)" class="button blue rounded">.blue</a>
      <a href="javascript:void(0)" class="button green rounded">.light-blue</a>
      <a href="javascript:void(0)" class="button deep-orange rounded">.cyan</a>
      <a href="javascript:void(0)" class="button blue-gray rounded">.teal</a>

      <hr />
    </div><!-- end [ CONTAINER > BUTTONS > ROUNDED ] -->

    <div class="container">
      <h3>Button <strong>Group</strong>:</h3>
      <p>
        EX: <code>{.buttons>.button.red+.button.red+.button.red}</code>
      </p>
      <div class="buttons">
        <a href="javascript:void(0)" class="button red">.red</a>
        <a href="javascript:void(0)" class="button red">.red</a>
        <a href="javascript:void(0)" class="button red">.red</a>
      </div>
      <p><br /></p>
      <h5>Button Group <i class="fa fa-angle-right"></i> <strong>Positions</strong> <small>[ left, center, right ]</small></h5>
      <p>
      Por padrão o posicionamento do grupo de botões é alinhado à esquerda, sendo assim desnecessário a utilização do data atributo {data-position="left"}</p>
      <p>
        EX: <code>{.buttons[data-position="left"]>.button.green+.button.green+.button.green}</code>
      </p>
      <div class="buttons" data-position="left">
        <a href="javascript:void(0)" class="button green">.green</a>
        <a href="javascript:void(0)" class="button green">.green</a>
        <a href="javascript:void(0)" class="button green">.green</a>
      </div>
      <p><br /></p>
      <p>
        EX: <code>{.buttons[data-position="center"]>.button.blue+.button.blue+.button.blue}</code>
      </p>
      <div class="buttons" data-position="center">
        <a href="javascript:void(0)" class="button blue">.blue</a>
        <a href="javascript:void(0)" class="button blue">.blue</a>
        <a href="javascript:void(0)" class="button blue">.blue</a>
      </div>
      <p><br /></p>
      <p>
        EX: <code>{.buttons[data-position="right"]>.button.pink+.button.pink+.button.pink}</code>
      </p>
      <div class="buttons" data-position="right">
        <a href="javascript:void(0)" class="button pink">.pink</a>
        <a href="javascript:void(0)" class="button pink">.pink</a>
        <a href="javascript:void(0)" class="button pink">.pink</a>
      </div>
      <hr />

      <h5>Button Group <i class="fa fa-angle-right"></i><strong> Size</strong> <small>{small, large}</small></h5>
      <p>
        EX: <code>{.buttons[data-size="small"]>.button.green+.button.green+.button.green}</code>
      </p>
      <div class="buttons" data-size="small">
        <a href="javascript:void(0)" class="button green">.green</a>
        <a href="javascript:void(0)" class="button green">.green</a>
        <a href="javascript:void(0)" class="button green">.green</a>
      </div>
      <p><br /></p>
      <p>
        EX: <code>{.buttons>.button.blue+.button.blue+.button.blue}</code>
      </p>
      <div class="buttons">
        <a href="javascript:void(0)" class="button blue">.blue</a>
        <a href="javascript:void(0)" class="button blue">.blue</a>
        <a href="javascript:void(0)" class="button blue">.blue</a>
      </div>
      <p><br /></p>
      <p>
        EX: <code>{.buttons[data-size="large"]>.button.pink+.button.pink+.button.pink}</code>
      </p>
      <div class="buttons" data-size="large">
        <a href="javascript:void(0)" class="button pink">.pink</a>
        <a href="javascript:void(0)" class="button pink">.pink</a>
        <a href="javascript:void(0)" class="button pink">.pink</a>
      </div>
    </div><!-- end [ CONTAINER > BUTTONS > GROUP ] -->
  </section>
</main>

<?php include('elements/footer.php');?>
